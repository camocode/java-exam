package com.camocode.test;

import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by wkoszut on 21/06/16.
 */

public class CoreJava {

    @Test
    public void sortObjects() {
        final String[] strings = {"z", "x", "y", "abc", "zzz", "zazzy"};
        final String[] expected = {"abc", "x", "y", "z", "zazzy", "zzz"};

        //todo please sort strings array in the simple possible way

        assertArrayEquals(expected, strings);
    }

    @Test
    public void arrayCopy() {
        int[] integers = {0, 1, 2, 3, 4};

        //todo expand existing array by 1


        assertEquals(6, integers.length);

        //todo set last element of array to 5

        assertEquals(5, integers[5]);
    }

    // todo Write an algorithm that prints all numbers between 1 and n,
    // replacing multiples of 3 with the String Bla, multiples of 5 with Blu, and multiples of 7 with BlaBlu
    @Test
    public void blaBlu() {
        String[] expctedResult = {"1", "2", "Bla", "4", "Blu", "Bla", "BlaBlu", "8", "Bla", "Blu"};


    }


    public static class ThreadPrinter implements Runnable {
        int num = 0;

        public ThreadPrinter(int num) {
            this.num = num;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                System.out.printf("Thread %n loop %n", num, i);
                try {
                    Thread.sleep(100l);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.printf("Thread %n ended.", num);
        }
    }

    @Test
    public void threadTester() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

//        todo Using Executor run 4 ThreadPrinter in parallel

    }

    @Test
    public void threadTesterSingleton() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

//        todo Using Executor run 4 ThreadPrinter in one by one
    }


    //    todo Implement HTTP requests in Java and print received content
    @Test
    public void makeBareHttpRequest() throws IOException {
        final URL url = new URL("http", "www.camocode.com", "/");
    }

}