package com.camocode.test;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by wkoszut on 21/06/16.
 */
public class SmallTasks {

    //todo A prime number (or a prime) is a natural number greater than 1 that has no positive divisors other than 1 and
    // itself. A natural number greater than 1 that is not a prime number is called a composite number.
    // For example, 5 is prime, as only 1 and 5 divide it, whereas 6 is composite, since it has the divisors 2 and 3
    // in addition to 1 and 6. The fundamental theorem of arithmetic establishes the central role of primes in number
    // theory: any integer greater than 1 can be expressed as a product of primes that is unique up to ordering.
    // This theorem requires excluding 1 as a prime.
    @Test
    public void isPrimeNumber() {
        assertEquals(true, isPrimeNumber(1));
        assertEquals(true, isPrimeNumber(19));
        assertEquals(false, isPrimeNumber(4));
        assertEquals(true, isPrimeNumber(11));
    }

    private boolean isPrimeNumber(int i) {

        return true;
    }

    //todo Write a program to reverse a string using recursive methods. You should not use any string reverse methods to do this.
    @Test
    public void isReferseString() {
        assertEquals("anna", reverseString("anna"));
        assertEquals("tset", reverseString("test"));
    }

    private String reverseString(String anna) {
        return null;
    }

    //todo Given a string, find the length of the longest substring without repeating characters. For example,
    // the longest substring without repeating letters for "abcabcbb" is "abc", which the length is 3. For "bbbbb"
    // the longest substring is "b", with the length of 1.
    @Test
    public void isLongestString() {
        assertEquals("abc", findLongetsString("abcabcbb"));
        assertEquals("b", findLongetsString("bbbbb"));
    }

    private String findLongetsString(String abcabcbb) {
        return null;
    }

}
